package nz.zhang;

/**
 * CallbackAble - interface that lets a class be passed as a callback for dynamic interaction.
 * <p/>
 * Created by G-Rath on 29/05/2015.
 */
public interface CallbackAble
{
	/**
	 * Callback {@code interface}. Note the "..." - this is nothing scary, it just means that the field is an ARRAY of potentially ANY size.
	 *
	 * "..." can be used on any data-type: {@code String...}, {@code int...}, {@code boolean...}, etc etc.
	 * By doing it as {@code Object...} it means that ANYTHING can be passed back of any type.
	 * This has the downside however of requiring typecasting (and checking using {@code instanceof}).
	 *
	 * @param caller This is an ID - the callback method should have logic that checks who the caller is, and then moves on from that.
	 * @param objects Array of any size (but at least one) that can be of any type.
	 */
	void callback(String caller, Object... objects);
}
