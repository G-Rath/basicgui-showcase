package nz.zhang.gui;

import nz.zhang.CallbackAble;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * MainFrontend
 * Created by G-Rath on 29/05/2015.
 */
public class MainFrontend extends JFrame implements CallbackAble
{
	private PanelButtonCollection buttonCollection;
	private PanelExampleList panelExampleList;

	private FrameDynamic[] frameDynamics = new FrameDynamic[4];

	private FrameDynamic zeroButtonDynamic;

	private JTextField inputField;

	public MainFrontend() throws HeadlessException
	{
		setupGUI();
	}

	@Override
	public void callback( String caller, Object... objects )
	{
		switch( caller )
		{
			case "button-collection-1":
				int buttonNumber = (int) objects[0];

				if( buttonNumber == 4 )
				{
					//Example of a simple text-enter dialog
					String textInput = (String) JOptionPane.showInputDialog( this,
					                                                     "enter text:",
					                                                     "text-input",
					                                                     JOptionPane.PLAIN_MESSAGE,
					                                                     null,
					                                                     null,
					                                                     "default" );

					panelExampleList.addToList( "User inputted text via dialog: " + textInput );
				}
				else
				{
					//ButtonNumber 3 makes a new frame EVERY TIME. the rest just show the their own frames each time.
					if( frameDynamics[buttonNumber] == null || buttonNumber == 3 )
						frameDynamics[buttonNumber] = new FrameDynamic();

					frameDynamics[buttonNumber].setTitle( "button #" + buttonNumber + " frame" );

					frameDynamics[buttonNumber].setVisible( true );
				}

				break;
		}
	}

	public void setupGUI()
	{
		panelExampleList = new PanelExampleList();
		buttonCollection = new PanelButtonCollection( "button-collection-1", this );

		inputField = new JTextField( 40 );
		JButton buttonSubmit = new JButton( "submit" );

		final JPanel inputPanel = new JPanel();
		inputPanel.setLayout( new BoxLayout( inputPanel, BoxLayout.LINE_AXIS ) );
		inputPanel.add( inputField );
		inputPanel.add( buttonSubmit );

		JPanel jPanel_Label_ConsoleCommand = new JPanel();
		jPanel_Label_ConsoleCommand.setLayout( new BoxLayout( jPanel_Label_ConsoleCommand, BoxLayout.LINE_AXIS ) );

		JLabel jLabel = new JLabel( "Input: " );
		jPanel_Label_ConsoleCommand.add( jLabel );
		jPanel_Label_ConsoleCommand.add( Box.createHorizontalGlue() );

		JPanel jPanel_InputPanel_And_Label = new JPanel();
		jPanel_InputPanel_And_Label.setLayout( new BoxLayout( jPanel_InputPanel_And_Label, BoxLayout.PAGE_AXIS ) );

		jPanel_InputPanel_And_Label.add( jPanel_Label_ConsoleCommand );
		jPanel_InputPanel_And_Label.add( inputPanel );

		JPanel mainPanel = new JPanel();

		/*
		Ok so when you run this you should note how the buttons stay at the top, and the other stuff doesn't (when you resize the panel).

		(note that panel can be replaced with "component" since not everything in a panel, but everything added is a component)
		(note when referring to weights with numbers the layout is as a vector: (x, y). Example: (0.5, 0.8) means weightx = 0.5, weighty = 0.8) )

		weights are interesting - they apply to both the panel and the components. A panels weight denotes in a % how much its effected by the resize.
		The components in said panel are then sized based on their weight (if using GridBagLayout).

		This is important because of ButtonCollection. The buttons in ButtonCollection we  don't want to resize y
		That is we don't want them to get taller or shorter, only wider. Because of this in the ButtonCollection panel the buttons are given a weight
		of (0.5, 0).
		The reason you might get confused is that here we give the buttonCollection a weight of (0,0). This is because we don't want the buttonPanel
		to move at all, but we still want it to resize.

		TRY: change the weighty of buttonCollection to 0.5, and see how the panel resizes, giving a massive blank gap while the buttons remain the same.
		TRY: change the weighty of the createVerticalStrut - right now the panelExampleList gets MOST (not all) of the resize %, so it grows bigger faster.
		 */

		mainPanel.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;
		c.weighty = 0.5;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 0;
		mainPanel.add( buttonCollection, c );

		c.weightx = 0.01;
		c.weighty = 0.01;
		c.gridy = 1;
		mainPanel.add( Box.createVerticalStrut( 10 ), c );

		c.weightx = 0.5;
		c.weighty = 0.5;
		c.gridy = 2;
		mainPanel.add( panelExampleList, c );

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 3;
		mainPanel.add( jPanel_InputPanel_And_Label, c );

		buttonSubmit.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				submitButtonPressed();
			}
		} );
		inputField.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				submitButtonPressed();
			}
		} ); //This fires when the user pressed enter

		//inputPanel

		mainPanel.setBorder( new EmptyBorder( 10, 10, 10, 10 ) );

		this.setMinimumSize( new Dimension( 500, 500 ) );
		this.setPreferredSize( new Dimension( 1000, 500 ) );
		this.setTitle( "BasicGUI showcase - main panel" );
		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );
	}

	public void submitButtonPressed()
	{
		panelExampleList.addToList( inputField.getText() );
		inputField.setText( "" ); //Blank the input field
	}
}
