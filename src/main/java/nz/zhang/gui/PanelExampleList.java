package nz.zhang.gui;

import nz.zhang.gui.listmodels.ExampleModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ExampleListPanel - Example of a collection of GUI components being used in a class extending panel. Shows how JPanel extending classes can be
 * used to make reusable components with limited exposing of required methods (for variable access). Also shows how layout managed can be combined.
 * Created by G-Rath on 29/05/2015.
 */
public class PanelExampleList extends JPanel
{
	private JList<String> exampleList;
	/** A model is used similar to an array - It actually holds a class of type Collection, but exposes access with the Collection methods */
	private ExampleModel<String> exampleDisplayModel = new ExampleModel<>();

	public PanelExampleList()
	{
		setupGUI();
	}

	private void clearList()
	{
		exampleList.clearSelection();
		exampleDisplayModel.clear();
	}

	public void addToList( String message )
	{
		exampleDisplayModel.add( message );
	}

	public void setupGUI()
	{
		exampleList = new JList<>( exampleDisplayModel );
		exampleList.setFocusable( false );
		JScrollPane exampleListScroller = new JScrollPane( exampleList );

		JButton button_ClearList = new JButton( "clear" );
		JButton button_AnotherButton = new JButton( "Nothing" );

		JPanel panel = new JPanel();
		panel.setLayout( new BoxLayout( panel, BoxLayout.LINE_AXIS ) );

		panel.add( button_ClearList );
		panel.add( Box.createHorizontalGlue() ); //Glues take all the size they can get, forcing elements to either end of the component they are in.
		panel.add( button_AnotherButton );

		JPanel labelPanel = new JPanel();
		labelPanel.setLayout( new BoxLayout( labelPanel, BoxLayout.LINE_AXIS ) );
		labelPanel.add( new JLabel( "text-input-list:" ) );
		labelPanel.add( Box.createHorizontalGlue() ); //We want the label pushed to the left as far as possible

		this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );

		this.add( labelPanel );
		this.add( exampleListScroller );
		this.add( Box.createVerticalStrut( 5 )); //Struts are like fixed borders. unlike glues they resize with everyone else (glues take priority for resize)
		this.add( panel );
		this.add( Box.createVerticalStrut( 5 ) );

		button_ClearList.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				clearList();
			}
		} );
	}
}
