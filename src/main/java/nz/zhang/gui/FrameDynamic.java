package nz.zhang.gui;

import nz.zhang.CallbackAble;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * FrameDynamic
 * <p/>
 * Created by G-Rath on 29/05/2015.
 */
public class FrameDynamic extends JFrame implements CallbackAble
{
	private PanelButtonCollection buttonCollection;
	private PanelExampleList panelExampleList;

	public FrameDynamic() throws HeadlessException
	{
		setupGUI();
	}

	@Override
	public void callback( String caller, Object... objects )
	{
		switch( caller )
		{
			case "button-collection-2":
				int buttonNumber = (int) objects[0];
				panelExampleList.addToList( "button #" + buttonNumber + " was pressed" );
				break;
		}
	}

	public void setupGUI()
	{
		panelExampleList = new PanelExampleList();
		buttonCollection = new PanelButtonCollection( "button-collection-2", this );

		JPanel mainPanel = new JPanel();

		mainPanel.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;
		c.weighty = 0.5;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 0;
		mainPanel.add( buttonCollection, c );

		c.weightx = 0.2;
		c.weighty = 0.2;
		c.gridy = 1;
		mainPanel.add( Box.createVerticalStrut( 10 ), c );

		c.weightx = 0.5;
		c.weighty = 0.5;
		c.gridy = 2;
		mainPanel.add( panelExampleList, c );

		mainPanel.setBorder( new EmptyBorder( 10, 10, 10, 10 ) );

		this.setMinimumSize( new Dimension( 300, 300 ) );
		this.setPreferredSize( new Dimension( 300, 300 ) );
		this.setTitle( "BasicGUI showcase - main panel" );

		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );
	}
}
