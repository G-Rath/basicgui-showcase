package nz.zhang.gui;

import nz.zhang.CallbackAble;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * PanelButtonCollection
 * <p/>
 * Created by G-Rath on 29/05/2015.
 */
public class PanelButtonCollection extends JPanel
{
	private ArrayList<JButton> buttonList = new ArrayList<>();

	/** This tag is passed as the caller when we callback so that the caller knows who we are. this should be unique for each tag */
	private final String callbackTag;

	private CallbackAble callbackAble;

	public PanelButtonCollection( String callbackTag, CallbackAble callbackAble )
	{
		this.callbackTag = callbackTag;
		this.callbackAble = callbackAble; //this. is required since callbackAble is defined within scope. this.callbackAble is the global variable
		setupGUI();
	}

	private void callBack( Object... objects)
	{
		callbackAble.callback( callbackTag, objects );
	}

	public void aButtonPressed( JButton pressedButton )
	{
		int buttonNumber = Integer.parseInt( pressedButton.getText().substring( pressedButton.getText().indexOf( "#" ) + 1 ) );

		//This second object (string "pressed") isn't really needed, but it shows how the "..." array works.
		callBack( buttonNumber, "pressed" );
	}

	public void setupGUI()
	{
		this.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		//Weight is about resizing - if a component has a weightx of 0.5 then it gets greater weight on resize on how much it gets resized.
		//A component with a weight of 0 means it won't resize. In this case, we don't want the buttons to resize down, so they have weighty = 0
		//This can take a little to get use to, because of the layering of GUI. weighty = 0 means they won't change SIZE on reszie.
		//They can still move if other components resize. Its best just to think of each weight as separate from the rest, and with the mindset of
		//"how much do i want this component to reszie as a % relative to the other components weight in this object?"
		//(I.e. don't think about the "other" components in other classes like MainFrontend)
		c.weightx = 0.5;
		c.weighty = 0;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0; //GridBagLayout works with a grid (surprise!). This grid is as big as you want, and is sized as you give grid co-ords
		c.gridy = 0; //For example if you set gridx to 3, without adding a component in space 1 and 2 that doesn't matter, it will work just fine.

		for( int i = 0; i < 5; i++ )
		{
			final JButton newButton = new JButton( "#" + i );

			c.gridx = i;

			this.add( newButton, c  );

			newButton.addActionListener( new ActionListener()
			{
				@Override
				public void actionPerformed( ActionEvent e )
				{
					aButtonPressed( newButton );
				}
			} );
		}
	}
}
