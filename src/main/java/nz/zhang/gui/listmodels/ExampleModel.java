package nz.zhang.gui.listmodels;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * ExampleModel - List model that extends AbstractListModel. This doesn't actually do much vs the default (DefaultListModel) but this way you can
 * add additional logic code in the future.
 * <p/>
 * Created by G-Rath on 29/05/2015.
 */
public class ExampleModel<T> extends AbstractListModel<T>
{
	private List<T> list;

	public ExampleModel( List<T> list )
	{
		this.list = list;
	}

	public ExampleModel()
	{
		this.list = new ArrayList<>();
	}

	@Override
	public int getSize()
	{
		return list.size();
	}

	@Override
	public T getElementAt( int i )
	{
		return list.get( i );
	}

	/**
	 * Adds an element to the list
	 *
	 * @param data element to add
	 */
	public void add( T data )
	{
		list.add( data );
		fireIntervalAdded( this, 0, getSize() ); //This fires off an event chain that gets any lists using this model to update themselves.
	}

	/**
	 * Add all the elements from the given list into this model's list
	 *
	 * @param elementsToAdd list of elements to be added
	 */
	public void addAll( List<T> elementsToAdd )
	{
		list.addAll( elementsToAdd );
		fireIntervalAdded( this, 0, getSize() ); //This fires off an event chain that gets any lists using this model to update themselves.
	}

	/** Clears the model of all elements */
	public void clear()
	{
		list.clear();
		fireIntervalAdded( this, 0, getSize() ); //This fires off an event chain that gets any lists using this model to update themselves.
	}
}
